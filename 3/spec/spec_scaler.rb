# frozen_string_literal: true

require_relative '../scaler'
require 'faker'

RSpec.describe Scaler do
  include Math
  describe '#check' do
    let(:b) { Faker::Number.decimal }
    let(:period) { (-2..2) }
    lambda1 = ->(x) { Math.sin(x) * x }
    lambda2 = ->(x) { Math.tan(x) }

    context 'checking processing of blocks and lambdas' do
      it 'both given' do
        expect(
          described_class.calculate_m(b, period, lambda1) { |x| Math.sin(x) * x }
        ).to eq(nil)
        expect(
          described_class.calculate_m(b, period, lambda2) { |x| Math.tan(x) }
        ).to eq(nil)
      end

      it 'given lambda' do
        expect(
          described_class.calculate_m(b, period, lambda1)
        ).to be_an(Numeric)
        expect(
          described_class.calculate_m(b, period, lambda2)
        ).to be_an(Numeric)
      end

      it 'both of methods are equal' do
        expect(
          described_class.calculate_m(b, period, lambda1)
        ).to eq(
          described_class.calculate_m(b, period) { |x| Math.sin(x) * x }
        )

        expect(
          described_class.calculate_m(b, period, lambda2)
        ).to eq(
          described_class.calculate_m(b, period) { |x| Math.tan(x) }
        )
      end
    end
  end
end
