# frozen_string_literal: true

require_relative 'scaler'

def enter_b
  puts 'Введите значение B: '
  b = gets.to_f
  period = (-2..2)

  # first func
  puts Scaler.calculate_m(b, period) { |x| Math.sin(x) * x }
  puts Scaler.calculate_m(b, period, ->(x) { Math.sin(x) * x })

  # second func
  puts Scaler.calculate_m(b, period) { |x| Math.tan(x) }
  puts Scaler.calculate_m(b, period, ->(x) { Math.tan(x) })
end

enter_b if __FILE__ == $PROGRAM_NAME
