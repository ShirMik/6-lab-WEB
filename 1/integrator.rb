# frozen_string_literal: true

# class that makes integration
class Integrator
  include Math
  include Enumerable

  def initialize
    @true_result = (1 - Math.cos(1)) / 2
  end

  def f(x)
    x * Math.sin(x**2)
  end

  def integrate_loops(accuracy)
    accuracy = accuracy.to_f if accuracy.is_a? Rational
    return unless accuracy.is_a? Float

    n = 0
    loop do
      n += 1
      h = 1.0 / n

      result = 0
      for i in (0..(n - 1)) do
        result += f(h / 2 + i.to_f * h)
      end
      result *= h

      # for debug: p "1: #{result}"
      return [result, n] if (@true_result - result).abs < accuracy
    end
  end

  def integrate_enum(accuracy)
    accuracy = accuracy.to_f if accuracy.is_a? Rational
    return unless accuracy.is_a? Float

    (1..(1.0 / 0)).lazy.collect { |n| (0..(n - 1)).sum { |i| f(1.0 / (2 * n) + i.to_f / n) } / n }
                  .find { |result| (@true_result - result).abs < accuracy }
  end
end
