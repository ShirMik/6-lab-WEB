# frozen_string_literal: true

require_relative '../integrator'
require 'faker'

RSpec.describe Integrator do
  describe '#check' do
    context 'when param is number' do
      it '10^ -4' do
        accuracy = 10**-4
        reference = [0.22975357251567788, 29]

        expect(described_class.new.integrate_loops(accuracy)).to eq reference
        expect((described_class.new.integrate_enum(accuracy) - reference[0]).abs < accuracy.to_f).to be_truthy
      end
      it '10^ -5' do
        accuracy = 10**-5
        reference = [0.22983895934056, 90]

        expect(described_class.new.integrate_loops(accuracy)).to eq reference
        expect((described_class.new.integrate_enum(accuracy) - reference[0]).abs < accuracy.to_f).to be_truthy
      end
    end
  end
end
